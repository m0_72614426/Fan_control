#include <reg52.h>
#include "main.h"	
#include "typedef.h"
#include "oled.h"
#include "drvdht11.h"
#include "delay.h"
#include "KEY.h"	
#include "relay.h"	
#include "motor.h"	

//---------------------------------------------------------------------------------------------------------------------------------------------
// 函 数 名: sys_init
// 功能说明: 系统初始化配置
// 形    参: 无
// 返 回 值: 无
// 日    期: 2025/1/20
// 作    者：张腾跃
// 备    注: 主要是一些外设的初始化，比如蜂鸣器、风扇、OLED显示器等
//---------------------------------------------------------------------------------------------------------------------------------------------
void sys_init(void)
{	
    BUZZER = 0; // 将蜂鸣器设置为关闭状态
    FAN_control(0);    // 将风扇关闭
    HOT = 0;    // 关闭加热状态

    // 初始化OLED显示器
    OLED_Init(); // 调用OLED初始化函数
    
    // 在OLED上显示初始状态信息
    OLED_ShowString(8, 0, "TEMP:  %C");    // 显示温度的提示信息
    OLED_ShowString(8, 2, "HUMI:  RH");     // 显示湿度的提示信息
    OLED_ShowString(8, 4, "MODE:");          // 显示工作模式的提示信息
    OLED_ShowString(8, 6, "ZTY");            // 显示特定标识
	  OLED_ShowString(48, 4, "STOP "); // 在OLED显示"停止"
    OLED_ShowString(48, 6, "STOP "); // 在OLED显示"停止" 
    OLED_ShowString(96, 6, "STOP "); // 在OLED显示"停止" 
}

/*******************************************************************************
* 函 数 名       : main
* 功能说明       : 主函数，程序入口
* 输    入       : 无
* 输    出       : 无
* 作    者       ：张腾跃
* 日    期       : 2025/1/19
*******************************************************************************/
void main(void)
{	
	sys_init(); // 调用系统初始化函数，设置初始状态

	while (1)
	{	  
		KEY_Mode();        // 检测按键状态，处理按键输入
		KEY_CLOSE_FAN();   // 检查风扇关闭条件
		dht11_data_read(); // 读取DHT11传感器的数据
		if(Tem_Turn_ADD==1&&Tem_Turn_SUBB==1)//判断是否需要进入温度上限更改模式
		{//不进入
			OLED_ShowString(8, 4, "MODE:");          // 显示工作模式的提示信息
			
			// 检测是否选择自动模式并且未停止
			if(MODE_choose == 1 && MODE_STOP == 0)
			{
				TEM_CON(); // 根据温度值控制风扇
			}
			
			// 检查是否需要转动电机
			if(MODE_turn == 1)
			{
					motor_turn(); // 控制电机转动
			}			
		}
		else
		{//进入
			FAN_control(0);           // 停止风扇
	  	motor_stop();      // 停止电机
		  HOT = 0;          // 关闭加热
			KEY_ADD_TEM();//温度加按键检测
			KEY_SUBB_TEM();//温度减按键检测
			temp_up=temp_turn;//温度更新检测
			OLED_ShowString(8, 4, "TEMH:");          // 显示工作模式的提示信息
			OLED_ShowString(64, 4, "UP  ");          // 显示工作模式的提示信息
			OLED_ShowNum(48, 4, temp_turn, 2, 16); // 显示温度
		}
	}
}

